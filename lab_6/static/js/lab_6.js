// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  if (x === 'ac') {
    print.value = "";
  } else if (x === 'eval') {
    print.value = Math.round(evil(print.value) * 10000) / 10000;
    erase = true;
  } else if (x === 'log') {
    print.value = Math.round(Math.log10(print.value) * 10000) / 10000;
  } else if (x === 'sin') {
    print.value = Math.round(Math.sin(print.value * Math.PI / 180) * 10000) / 10000;
  } else if (x === 'tan') {
    print.value = Math.round(Math.tan(print.value * Math.PI / 180) * 10000) / 10000;
  } else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}
// END

$(document).ready(function() {

  //Chat box
  $(".chat-text textarea").keypress(function(e) {
    if (e.which == 13 && !e.shiftKey) {
      $(".chat-body .msg-insert").append('<p class="msg-send">' + $(this).val() + '</p>');
      e.preventDefault();
      $(this).val('');
      $(".chat-body").animate({
        scrollTop: $(".chat-body")[0].scrollHeight
      }, 'fast');
    }
  });

  $(".enter-btn button").click(function() {
    $(".chat-body .msg-insert").append('<p class="msg-send">' + $(".chat-text textarea").val() + '</p>');
    $(".chat-text textarea").val('');
    $(".chat-body").animate({
      scrollTop: $(".chat-body")[0].scrollHeight
    }, 'fast');
  });
  //END

  //select2
  if (typeof(Storage) !== "undefined") {
    localStorage.themes = JSON.stringify([{
        "id": 0,
        "text": "Red",
        "bcgColor": "#F44336",
        "fontColor": "#FAFAFA"
      },
      {
        "id": 1,
        "text": "Pink",
        "bcgColor": "#E91E63",
        "fontColor": "#FAFAFA"
      },
      {
        "id": 2,
        "text": "Purple",
        "bcgColor": "#9C27B0",
        "fontColor": "#FAFAFA"
      },
      {
        "id": 3,
        "text": "Indigo",
        "bcgColor": "#3F51B5",
        "fontColor": "#FAFAFA"
      },
      {
        "id": 4,
        "text": "Blue",
        "bcgColor": "#2196F3",
        "fontColor": "#212121"
      },
      {
        "id": 5,
        "text": "Teal",
        "bcgColor": "#009688",
        "fontColor": "#212121"
      },
      {
        "id": 6,
        "text": "Lime",
        "bcgColor": "#CDDC39",
        "fontColor": "#212121"
      },
      {
        "id": 7,
        "text": "Yellow",
        "bcgColor": "#FFEB3B",
        "fontColor": "#212121"
      },
      {
        "id": 8,
        "text": "Amber",
        "bcgColor": "#FFC107",
        "fontColor": "#212121"
      },
      {
        "id": 9,
        "text": "Orange",
        "bcgColor": "#FF5722",
        "fontColor": "#212121"
      },
      {
        "id": 10,
        "text": "Brown",
        "bcgColor": "#795548",
        "fontColor": "#FAFAFA"
      }
    ]);
    if (!localStorage.selectedTheme)
      localStorage.selectedTheme = JSON.stringify({
        "id": 3,
        "text": "Indigo",
        "bcgColor": "#3F51B5",
        "fontColor": "#FAFAFA"
      });

  } else {
    alert("no local storage support");
  }


  var theme = JSON.parse(localStorage.selectedTheme);

  $("html").css({
    "background-color": theme.bcgColor,
    "color": theme.fontColor
  });
  $("body").css({
    "background-color": theme.bcgColor,
    "color": theme.fontColor
  });

  $('.my-select').select2({
    "data": JSON.parse(localStorage.themes)
  });

  $('.apply-button').on('click', function() { // sesuaikan class button
    var val = $('.my-select').val();

    function find(arr, id) {
      for (var i = 0; i < arr.length; i++) {
        if (arr[i].id == id) {
          return (arr[i]);
        }
      }
    }

    var theme = find(JSON.parse(localStorage.themes), val);

    $("html").css({
      "background-color": theme.bcgColor,
      "color": theme.fontColor
    });
    $("body").css({
      "background-color": theme.bcgColor,
      "color": theme.fontColor
    });

    localStorage.selectedTheme = JSON.stringify(theme);

  })

  //END


  //Hide Chat
  $('h2 + img').click(function(){
    $('.chat-box').toggleClass('off');
  });
  //END

});
