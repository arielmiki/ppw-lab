from django.shortcuts import render

# Create your views here.
response = {'author':'Ariel Miki Abraham'}
def index(request):
    html = 'lab_8/lab_8.html'

    return render(request,html,response)
